#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <windows.h>

#define High 20 
#define Width 30

int moveDirection;
int canvas[High][Width]={0};
void gotoxy(int x,int y)
{
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD pos;
	pos.X = x;
	pos.Y = y;
	SetConsoleCursorPosition(handle,pos); 
}
void moveSnakeByDirection()
{
	int i,j;
	for (i=1;i<High-1;i++)
	    for(j=1;j<Width-1;j++)
	        if(canvas[i][j]>0)
	           canvas[i][j]++;
	int oldTail_i,oldTail_j,oldHead_i,oldHead_j;
	int max=0;
	for(i=1;i<High-1;i++)
	    for(j=1;j<Width-1;j++)
		    if(canvas[i][j]>0)
			{
				if(max<canvas[i][j])
				{
					max=canvas[i][j];
					oldTail_i=i;
					oldTail_j=j;
				}
				if(canvas[i][j]==2)
				{
					oldHead_i=i;
					oldHead_j=j;
				}
			}
	canvas[oldTail_i][oldTail_j]=0;
	if(moveDirection==1)
	   canvas[oldHead_i-1][oldHead_j]=1;
	if(moveDirection==2)
	   canvas[oldHead_i+1][oldHead_j]=1;   
	if(moveDirection==3)
	   canvas[oldHead_i][oldHead_j-1]=1; 
	if(moveDirection==4)
	   canvas[oldHead_i][oldHead_j+1]=1;         		           
}
void startup()
{
	int i,j;
	for(i=1;i<High;i++)
	{
		canvas[i][0]=-1;
		canvas[i][Width-1]=-1;
	}
	for(j=0;j<Width;j++)
	{
		canvas[0][j]=-1;
		canvas[High-1][j]=-1;
	}
	canvas[High/2][Width/2]=1;
	for(i=1;i<=4;i++)
	    canvas[High/2][Width/2-i]=i+1;
	moveDirection=4;    
}
void show()\
{
	gotoxy(0,0);
	int i,j;
	for(i=0;i<High;i++)
	{
		for(j=0;j<Width;j++)
		{
			if(canvas[i][j]==0)
			   printf(" ");
			else if(canvas[i][j]==-1)
			   printf("#");
			else if(canvas[i][j]==1)
			   printf("@");
			else if(canvas[i][j]>1)
			   printf("*");      
		}
		printf("\n");
	}
	Sleep(100);
}

void updateWithoutInput()
{
	moveSnakeByDirection();
}
void updateWithInput()
{
}
int main()
{
	startup();
	while(1)
	{
		show();
		updateWithoutInput();
		updateWithInput();
	}
	return 0;
}