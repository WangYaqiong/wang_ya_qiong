#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>
void gotoxy(int x, int y) 
{
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD pos;
	pos.X = x;
	pos.Y = y;
	SetConsoleCursorPosition(handle, pos);
}
void HideCursor()	  
{
	CONSOLE_CURSOR_INFO cursor_info = { 1, 0 };
	SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE), &cursor_info);
}


int high;
int width;

int bird_x;
int bird_y;

int bar1_y;
int bar1_xTop;
int bar1_xDown;
int score;

void startup() 
{

	high = 15;	
	width = 25;

	bird_x = high / 2;
	bird_y = width / 4;

	bar1_y = width - 1;	
	bar1_xTop = high / 4;
	bar1_xDown = high / 2;

	score = 0;
}

void show() 
{
	gotoxy(0, 0);	
	int i, j;
	for (i = 0; i < high; i++)
	{
		for (j = 0; j < width; j++)
		{
			if ((i == bird_x) && (j == bird_y))	
			{
				printf("@");
			}
			else if ((j == bar1_y) && ((i < bar1_xTop) || (i > bar1_xDown)))	
			{
				printf("*");
			}
			else
			{
				printf(" ");
			}
		}
		printf("\n");
	}
	printf("score：%d\n", score);
}

int updateWithoutInput()
{
	bird_x++;	
	bar1_y--;
	if (bird_y == bar1_y)
	{
		if ((bird_x >= bar1_xTop) && (bird_x <= bar1_xDown))	
		{
			score++;
		}
		else
		{
			printf("Game failed！！！");
			return -1;
		}
	}
	else
	{
		if (bird_x > high)
		{
			printf("gamefailed！！！");
			return -1;
		}
	}
	if (bar1_y <= 0)
	{
		bar1_y = width - 1;
		int upside = rand() % (int)(high * 0.6) + 1;
		bar1_xTop = upside;
		int opening = rand() % (int)(high * 0.2) + 2;
		while ((bar1_xDown = bar1_xTop + opening) > high - 2)
		{
			opening = rand() % (int)(high * 0.2) + 2;
		}
	}
	Sleep(150);
	return 0;
 }
 
void updateWithInput()
{
	char input;
	if (_kbhit())
	{
		input = _getch();
		if (input == ' ' && bird_x > 0)
		{
			bird_x = bird_x - 2;
		}
	}
}
int main()
{
	srand((unsigned)time(NULL));
	HideCursor();
again:
	startup();
	while (1)
	{
		show();
		int ret = updateWithoutInput();
		if (ret == -1)
		{
			system("CLS");
			printf("1.restard\n0.exit\nchoose please：");
			int input = 0;
			scanf("%d", &input);
			if (input)
			{
				goto again;
			}
			else
				return 0;
		}
		updateWithInput();
	}
	return 0;
}
